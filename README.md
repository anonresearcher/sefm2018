# SEFM 2018
This repository contains a distribution of Canopy as well as the complete result tables and raw data presented in our SEFM 2018 submission: *Monte Carlo Tree Search for Finding Costly Paths in Programs*.

The repository is structured as follows:

* `canopy_dist.tar.gz`: Is a Gzip containing a Canopy distribution, including Canopy itself and its dependencies jpf-core and jpf-symbc. The source code of the examples used in the evaluation as well as the corresponding JPF files can be found in the `canopy/src/examples` directory.
* `./results`: Contains a PDF of the full results table that could not fit in the paper. It also contains the raw data for the tables in CSV files---`results_summary.csv` contains the the summary of the raw data used for producing the tables.


## Installation
We provide two ways for installing Canopy:

* Virtual machine with Docker 
* Installation on local machine by setting up manually Java PathFinder and Symbolic PathFinder

For reproducing the results, we **recommend** using the Docker installation.

### Docker
Assuming you have [Docker](https://www.docker.com/) installed, simply run:

```bash
$ docker build -t canopy .
# Will take some time to build the image...
$ docker run -it canopy
```

**Note** that, because there is no X11 available, the live views that can optionally be enabled for Canopy do not work. Therefore, please ensure that the configuration `canopy.livestats` in the JPF file is set to `false` to prevent crashes when run in the docker. Note that the results are still produced for the user to inspect if `canopy.stats=true`. 

If you would like to have the live views, please install Canopy locally as described in the next section.

### Local Machine
Create an installation folder where jpf-core, jpf-symbc and Canopy will reside. In the following, we will call this folder `WORKSPACE`.

First, extract the contents of `canopy_dist.tar.gz` to `WORKSPACE`:
```
$ mv canopy_dist.tar.gz WORKSPACE
$ cd WORKSPACE
$ tar xvf canopy_dist.tar.gz
```

To install Canopy, create `~/.jpf/site.properties` and set the `jpf-core`, `jpf-symbc` and `canopy` variables to point to the directory of your Canopy installation in `WORKSPACE`. 
```
jpf-core=/path/to/WORKSPACE/jpf-core
jpf-symbc=/path/to/WORKSPACE/jpf-symbc
canopy=/path/to/WORKSPACE/canopy

extensions=${jpf-core},${jpf-symbc}
```

This completes the installation of Canopy. 

In addition you have to install Z3 4.4.1. In order to do that, please consult the Z3 documentation: [Z3 on GitHub](https://github.com/Z3Prover/z3). You can download a release distribution of Z3 4.4.1 on the GitHub website.
After Z3 has been installed, you have to update your `LD_LIBRARY_PATH` (Linux) or `DYLD_LIBRARY_PATH` (OS X) to point to the installation `bin` directory of the Z3 installation, e.g., `z3/bin`. Then copy the Java bindings `com.microsoft.z3.jar` to `jpf-symbc/lib`.


## Usage 
The analysis can be performed by executing the JPF config file that specifies the parameters of the analysis, the constraint solver, the entry point of the system under analysis etc:

```
$ cd WORKSPACE
$ ./jpf-core/bin/jpf <path-to-jpf-file>
```

All JPF files for analyzing the examples are located in `canopy/src/examples/sefm2018/TECHNIQUE/` where `TECHNIQUE` is `mcts`,`mc`,`rl`, or `exhaustive`.

For example, assuming you are using the Docker installation, analysis of Quicksort with Canopy can be done by executing the following (ensure you are in the `WORKSPACE` directory):

For MCTS analysis:
```
$ ./jpf-core/bin/jpf canopy/src/examples/mcts/QuickSort_9.jpf
```

For Monte Carlo analysis:
```
$ ./jpf-core/bin/jpf canopy/src/examples/mc/QuickSort_9.jpf
```

For Reinforcement Learning analysis:
```
$ ./jpf-core/bin/jpf canopy/src/examples/rl/QuickSort_9.jpf
```

For Exhaustive analysis:
```
$ ./jpf-core/bin/jpf canopy/src/examples/exhaustive/QuickSort_9.jpf
```


### Generating batch results
To produce the dataset used for generating the results in the paper, we use the Batch Processor utility tool: it executes MCTS, RL, MC, or exhaustive a number of times (50 was used in the paper---except for exhaustive, which is just executed once) with different seeds and output all the statistics used for producing the tables in the paper.

To run the batch processor, use:
```
$ cd canopy/
$ ./tools/runBatch.sh <dir with JPF files or specific JPF file> <output folder> <comma separated list of ANALYSIS-CONFIG>
```

The Batch Processor will output a `.csv` file with the same filename prefix as the `.jpf` file being executed. The file will be stored in the `output folder` specified on the command line.

Following `ANALYSIS-CONFIG`s can be used with the Batch Processor. 



| MCTS (pruning)  | MCTS (pruning, no caching) | MCTS (no pruning)  | RL  | MC  | Exhaustive  |
|---|---|---|---|---|---|
|  mcts2p       | mcts2pnc  | mcts2     | rl250-05-05  | mcp  | exhaustive  |
|  mcts5p       | mcts5pnc  | mcts5     | rl100-05-05  | mc   |   |
|  mcts10p      | mcts10pnc | mcts10    | rl10-05-05   |   |   |
|  mcts20p      | mcts20pnc | mcts20    | rl100-01-01  |   |   |
|  mcts50p      | mcts50pnc | mcts50    | rl100-09-09  |   |   |
|  mcts100p     | mcts100pnc| mcts100   | rl100-09-01  |   |   |
|     | |         |   rl100-01-09  |||
|     | |         |   rl1-01-01    |||
|     | |         |   rl1-05-05    |||

In the table, `mcts2pnc` means MCTS, bias = sqrt(2), pruning, no constraints caching. `mcts5p` means MCTS, bias = 5, pruning, (constraints caching enabled). `mcts10` means MCTS, bias = 10, (no pruning), (constraints caching enabled).

`mcp` means Monte Carlo, with pruning. `exhaustive` means exhaustive analysis.

`rl250-05-05` means Reinforcement Learning, samples per optimization = 250, epsilon = 0.5, history weight = 0.5. `rl100-09-01` means Reinforcement Learning, samples per optimization = 100, epsilon = 0.9, history weight = 0.1. Pruning and constraints caching are enabled for all Reinforcement Learning configurations.


The JPF files that can be used with the Batch Processor are located in: `canopy/src/examples/batch`. This directory has three sub-directories: `increasingsize`, `incremental` and `noincremental`. The latter two contains JPF files for reproducing the results where incremental solving is used and not used, respectively.

For example, to generate the results for Quicksort for MCTS bias = 5, with pruning, constraints caching and incremental solving, execute the following:
```
$ ./tools/runBatch.sh src/examples/sefm2018/batch/incremental/QuickSort_9.jpf ./ mcts5p
```

To generate the results for Quicksort for MCTS bias = 5, with pruning, no constraints caching and no incremental solving, execute the following:
```
$ ./tools/runBatch.sh src/examples/sefm2018/batch/noincremental/QuickSort_9.jpf ./ mcts5pnc
```

The sub-directory, `increasingsize`, contains JPF files for reproducing the results in the plots in Figure 4 - 10. In order to generate the data set for these plots, use the script 

```
$ cd canopy
$ ./tools/runBatchIncreasingInputSize.sh <dir with JPF files or specific JPF file> <output folder> <comma separated list of ANALYSIS-CONFIG> <start input size> <end input size> <input size increment> <iterations>
```

The usage of this script is the same as the `runBatch.sh` script used before, but it takes 4 additional inputs. It will execute `iterations` number of times each input size from start to end with the specified input size increment. Similarly, the results will be output to a CSV file with the name corresponding to the prefix of the JPF file being analyzed. The CSV file contains an additional column for the input size being analyzed.

For example, to generate the results in the paper for TextCrunchr, execute the following:
```
$ ./tools/runBatchIncreasingInputSize.sh src/examples/sefm2018/batch/increasingsize/tc3.jpf ./ mcts5p 1 30 1 25
```
This will analyze TextCrunchr for all input sizes 1-30 with 25 different seeds using MCTS bias = 5 with pruning and incremental solving.


## Configuration of Canopy

Canopy has global and analysis local configuration options. Configuration of Canopy happens through the jpf file.

To enable Canopy, the JPF file **must** contain the `@using` directive:
```
@using canopy
```

Canopy relies on the configuration options available in Java PathFinder and Symbolic PathFinder and can use the incremental solver in Symbolic PathFinder. Please consult these projects regarding configuration.

Most important is that the JPF file contains values for (which are inherited from jpf-core and jpf-symbc):

* **target** Specifies the entry point of the SUT
* **classpath** Specifies the classpath of the SUT
* **symbolic.method** Specifies the method to be symbolically executed
* **symbolic.dp** Specifies the decision procedure to use for symbolic execution. Use `z3` for non-incremental solving. Use `z3bitvector` for z3 with bitvectors. Use `z3inc` and `z3bitvectorinc` for adding incremental solving (in addition specify: `listener=gov.nasa.jpf.symbc.numeric.solvers.IncrementalListener`)


### Global Options

All options are either optional or have default values.

* **canopy.rewardfunc** Specifies the reward function being used. An implementation of `canopy.reward.RewardFunction` that provides rewards for paths. Default: `canopy.reward.DepthRewardFunction`, i.e. reward is based on depth (number of decisions) of paths. To use the reward function that uses memory allocations (as used in section 5.5. in the paper), use `canopy.reward.AllocationRewardFunction`
* **canopy.termination** Specifies the termination strategy being used. An implementation of `canopy.termination.TerminationStrategy` that specifies when to stop sampling paths. Note that when pruning is used, analysis will terminate after all paths have been explored. Default: `canopy.termination.NeverTerminateStrategy`. See option `canopy.termination.samplingsize` which provides a shortcut for sampling a specific number of paths
* **canopy.termination.samplingsize** Specifies the number of samples to perform
* **canopy.livestats** Boolean that controls whether the live view will be shown to the user. If true, this can impact performance slightly. Default: true
* **canopy.stats** Boolean that controls whether to output results to std output when the analysis is done. Default: True
* **canopy.seed** Specifies the seed for the random number generators. **Note** If this option is not set, a default seed will be used
* **canopy.random** A boolean that controls whether the random number generators are initialized with random seeds. Default: False

### Analysis Local Options

The analyses are enabled by using the corresponding JPF Shell. Please consult each section below for how to use and configure the analyses.


#### Monte Carlo Tree Search

To enable this analysis, put in the JPF file:
```
shell = canopy.mcts.MCTSShell
```

The Monte Carlo Tree Search strategy can be configured with the following options:

* **canopy.mcts.uct.bias** Controls the UCT bias. Default is `Math.sqrt(2)`

#### Reinforcement Learning

To enable this analysis, put in the JPF file:
```
shell = canopy.reinforcement.ReinforcementLearningShell
```

The Reinforcement Learning strategy can be configured with the following options:

* **canopy.rl.samplesperoptimization** Controls how many paths will be sampled before performing the optimization step. Default 100
* **canopy.rl.epsilon** Sets the value of the epsilon parameter. Default: 0.5
* **canopy.rl.history** Sets the value of the history parameter. Default: 0.5

This analysis can also use model counting for amplifying rewards. This feature is currently experimental and is thus not covered here.


#### Pure Monte Carlo

To enable this analysis, put in the JPF file:
```
shell = canopy.montecarlo.MonteCarloShell
```

#### Exhaustive

To enable this analysis, put in the JPF file:
```
shell = canopy.exhaustive.ExhaustiveShell
```

