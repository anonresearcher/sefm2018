FROM ubuntu:14.04

#############################################################################
# Setup base image 
#############################################################################
RUN \
  apt-get update -y && \
  apt-get install software-properties-common -y && \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository ppa:webupd8team/java -y && \
  apt-get update -y && \
  apt-get install -y oracle-java8-installer
# Cut it in two---java takes a long time to install
RUN  apt-get install -y unzip \
                        graphviz \
                        ant \
                        libxtst6 \
                        openssh-server \
                        build-essential \
                        bison \
                        flex \
                        vim && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer

#############################################################################
# Environment 
#############################################################################

# set java env
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV JUNIT_HOME /usr/share/java

# Make dir for distribution
WORKDIR /
RUN mkdir tools
ENV WORKSPACE_DIR /tools

#############################################################################
# Dependencies 
#############################################################################

# Install Z3
WORKDIR ${WORKSPACE_DIR}
# Note that we specify a specific *release* of Z3
RUN wget https://github.com/Z3Prover/z3/releases/download/z3-4.4.1/z3-4.4.1-x64-ubuntu-14.04.zip 
RUN unzip z3-4.4.1-x64-ubuntu-14.04.zip && \
        rm z3-4.4.1-x64-ubuntu-14.04.zip
RUN ln -s z3-4.4.1-x64-ubuntu-14.04 z3

#############################################################################
# Install and configure jpf-related tools 
#############################################################################

# Copy tool collection and extract
ADD canopy_dist.tar.gz ${WORKSPACE_DIR}/

# This is messed up. For some weird reason
# setting LD_LIBRARY_PATH to the Z3 installation
# does not work---we have to symlink libz3java to jpf-symbc/lib *sigh*
#RUN rm ${WORKSPACE_DIR}/jpf-symbc/lib/libz3java.so
RUN ln -s ${WORKSPACE_DIR}/z3/bin/libz3java.so ${WORKSPACE_DIR}/jpf-symbc/lib/
ENV LD_LIBRARY_PATH ${WORKSPACE_DIR}/z3/bin

# Copy z3 java bindings
RUN rm ${WORKSPACE_DIR}/jpf-symbc/lib/com.microsoft.z3.jar
RUN cp ${WORKSPACE_DIR}/z3/bin/com.microsoft.z3.jar ${WORKSPACE_DIR}/jpf-symbc/lib/

# Set up jpf conf 
RUN mkdir /root/.jpf
RUN echo "jpf-core = ${WORKSPACE_DIR}/jpf-core" >> /root/.jpf/site.properties
RUN echo "jpf-symbc = ${WORKSPACE_DIR}/jpf-symbc" >> /root/.jpf/site.properties
RUN echo "canopy = ${WORKSPACE_DIR}/canopy" >> /root/.jpf/site.properties

# Set extensions var
RUN echo "extensions=\${jpf-core},\${jpf-symbc}" >> /root/.jpf/site.properties

# Let's go!
WORKDIR ${WORKSPACE_DIR}

